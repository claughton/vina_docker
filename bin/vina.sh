#!/bin/bash
# Wrapper around vina to suppress progress meter output which messes docker
#
for arg in $@
do
    if [[ "--help --help_advanced --version" == *"$arg"* ]]; then
        vina "$@"
        exit
    fi
done
vina "$@" > /dev/null
