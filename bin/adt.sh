#!/usr/bin/env bash
utilities=/usr/local/lib/python2.7/site-packages/AutoDockTools/Utilities24/
if [[ "$1" == -h || "$#" == 0 ]]; then
    echo "adt: a subset of AutoDock Tools."
    echo "     type 'adt <toolname.py>' to use, where <toolname.py> is one of:"
    if [[ -e "$utilities" ]]; then
        cd $utilities 
        ls *.py | grep -v __ 
    fi
    exit
fi
script=$utilities"$@"
python $script
