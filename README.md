# vina_docker

Delivers [AutoDock Vina](http://vina.scripps.edu/index.html) and the command-line tools (no GUI) from [AutoDock Tools](http://autodock.scripps.edu/resources/adt) via a Docker container.

## Installation

Easiest via pip:

```
pip install git+https://bitbucket.org/claughton/vina_docker.git
```
This installs two scripts in your path:

* vina : AutoDock Vina
* adt : Access to the Python scripts for preparation and analysis of Vina docking files.

## Usage

The `vina` command works just like the normal installed version of the software:

```
$ vina --help

Input:
  --receptor arg        rigid part of the receptor (PDBQT)
  --flex arg            flexible side chains, if any (PDBQT)
  --ligand arg          ligand (PDBQT)

Search space (required):
  --center_x arg        X coordinate of the center
  --center_y arg        Y coordinate of the center
  --center_z arg        Z coordinate of the center
  --size_x arg          size in the X dimension (Angstroms)
  --size_y arg          size in the Y dimension (Angstroms)
  --size_z arg          size in the Z dimension (Angstroms)

Output (optional):
  --out arg             output models (PDBQT), the default is chosen based on 
                        the ligand file name
  --log arg             optionally, write log file

Misc (optional):
  --cpu arg                 the number of CPUs to use (the default is to try to
                            detect the number of CPUs or, failing that, use 1)
  --seed arg                explicit random seed
  --exhaustiveness arg (=8) exhaustiveness of the global search (roughly 
                            proportional to time): 1+
  --num_modes arg (=9)      maximum number of binding modes to generate
  --energy_range arg (=3)   maximum energy difference between the best binding 
                            mode and the worst one displayed (kcal/mol)

Configuration file (optional):
  --config arg          the above options can be put here

Information (optional):
  --help                display usage summary
  --help_advanced       display usage summary with advanced options
  --version             display program version

$ 
```

The `adt` command gives you access to the Python scripts shipped as part of the full AutoDock Tools application. 
Most importantly, this includes the scripts `prepare_receptor4.py` and `prepare_liagnd4.py` used to prepare receptor
and ligand input files (typically PDB format) for use with AutoDock Vina (converting them to PDBQT format), but many
other utilitie are also available. To get a list:

```
$ adt
compute_AutoDock41_score.py
compute_consensus_maps_from_dlgs.py
compute_rms_between_methods.py
dpf3_to_dpf4.py
energy_average_maps.py
gpf3_to_gpf4.py
gpf4_to_gpf3.py
pdbq_to_pdbqt.py
pdbqs_to_pdbqt.py
pdbqt_to_pdb.py
pdbqt_to_pdbq.py
pdbqt_to_pdbqs.py
prepare_covalent_flexres.py
prepare_dpf.py
prepare_dpf4.py
prepare_dpf41.py
prepare_dpf42.py
prepare_flexreceptor4.py
prepare_gpf.py
prepare_gpf4.py
prepare_ligand.py
prepare_ligand4.py
prepare_ligand_dict.py
prepare_receptor.py
prepare_receptor4.py
repair_ligand4.py
rotate_molecule.py
summarize_docking.py
summarize_docking_directory.py
summarize_results.py
summarize_results4.py
summarize_time.py
summarize_wcg_docking.py
superimpose_based_on_subset.py
superpose_molecules.py
write_all_complexes.py
write_clustering_histogram_postscript.py
write_component_energies.py
write_conformations_from_dlg.py
write_largest_cluster_ligand.py
write_lowest_energy_ligand.py
write_random_state_ligand.py
write_rigid_ligand.py
write_vs_hits.py
```
To run a specific command:

```
$ adt prepare_receptor4.py -h
prepare_receptor4.py: option -h not recognized
Usage: prepare_receptor4.py -r filename

    Description of command...
         -r   receptor_filename 
        supported file types include pdb,mol2,pdbq,pdbqs,pdbqt, possibly pqr,cif
    Optional parameters:
        [-v]  verbose output (default is minimal output)
        [-o pdbqt_filename]  (default is 'molecule_name.pdbqt')
        [-A]  type(s) of repairs to make: 
             'bonds_hydrogens': build bonds and add hydrogens 
             'bonds': build a single bond from each atom with no bonds to its closest neighbor
             'hydrogens': add hydrogens
             'checkhydrogens': add hydrogens only if there are none already
             'None': do not make any repairs 
             (default is 'checkhydrogens')
        [-C]  preserve all input charges ie do not add new charges 
             (default is addition of gasteiger charges)
        [-p]  preserve input charges on specific atom types, eg -p Zn -p Fe
        [-U]  cleanup type:
             'nphs': merge charges and remove non-polar hydrogens
             'lps': merge charges and remove lone pairs
             'waters': remove water residues
             'nonstdres': remove chains composed entirely of residues of
                      types other than the standard 20 amino acids
             'deleteAltB': remove XX@B atoms and rename XX@A atoms->XX
             (default is 'nphs_lps_waters_nonstdres') 
        [-e]  delete every nonstd residue from any chain
              'True': any residue whose name is not in this list:
                      ['CYS','ILE','SER','VAL','GLN','LYS','ASN', 
                      'PRO','THR','PHE','ALA','HIS','GLY','ASP', 
                      'LEU', 'ARG', 'TRP', 'GLU', 'TYR','MET', 
                      'HID', 'HSP', 'HIE', 'HIP', 'CYX', 'CSS']
              will be deleted from any chain. 
              NB: there are no  nucleic acid residue names at all 
              in the list and no metals. 
             (default is False which means not to do this)
        [-M]  interactive 
             (default is 'automatic': outputfile is written with no further user input)
$ 
```

## License

Please see the web pages for [AutoDock Vina](http://vina.scripps.edu/index.html) and [AutoDock Tools](http://autodock.scripps.edu/resources/adt)  for their license details.





