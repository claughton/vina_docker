PREFIX ?= /usr/local
VERSION = "1.1.2"

all: install

install:
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	install -m 0755 scripts/vina $(DESTDIR)$(PREFIX)/bin/vina
	install -m 0755 scripts/adt $(DESTDIR)$(PREFIX)/bin/adt

uninstall:
	@$(RM) $(DESTDIR)$(PREFIX)/bin/vina
	@$(RM) $(DESTDIR)$(PREFIX)/bin/adt
	@docker rmi claughton/autodock-vina:$(VERSION)

build: 
	@docker build -t claughton/autodock-vina:$(VERSION) -f Dockerfile . 
	@docker tag claughton/autodock-vina:$(VERSION) claughton/autodock-vina:latest

publish: build
	@docker push claughton/autodock-vina:$(VERSION) 
	@docker push claughton/autodock-vina:latest 

.PHONY: all install uninstall build publish 
