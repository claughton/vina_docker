FROM ubuntu:latest
RUN apt-get update && apt-get install -y tar curl python && \
    curl -O http://vina.scripps.edu/download/autodock_vina_1_1_2_linux_x86.tgz && \
    tar xzf autodock_vina_1_1_2_linux_x86.tgz && \
    mkdir -p /data
COPY site-packages /usr/local/lib/python2.7/site-packages
ADD bin/vina.sh /usr/local/bin/vina.sh
ADD bin/adt.sh /usr/local/bin/adt.sh
ADD bin/run /usr/local/bin/run
ENV PATH $PATH:/autodock_vina_1_1_2_linux_x86/bin
ENV PYTHONPATH /usr/local/lib/python2.7/site-packages
ENTRYPOINT ["/usr/local/bin/vina.sh"]
CMD ["--help"]
