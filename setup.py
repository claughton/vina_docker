from setuptools import setup, find_packages
setup(
    name = 'vinadocker',
    version = '1.1.2',
    packages = find_packages(),
    scripts = [
        'scripts/vina',
        'scripts/adt',
    ],
)
